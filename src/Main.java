import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);
        System.out.println("Wpisz sciezke pliku: ");
        String sourcePath = sc.nextLine();
        System.out.println("Wpisz sciezke docelowa: ");
        String targetPath = sc.nextLine();
        copyFile(sourcePath, targetPath);

        copyFile(sourcePath, targetPath);
    }

    private static void copyFile(String sourcePath, String targetPath) throws IOException {

        FileOutputStream outputStream = new FileOutputStream(new File(targetPath));

        Scanner sc = new Scanner(new File(sourcePath));

        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            line += "\n";
            outputStream.write(line.getBytes());
        }
    }
}